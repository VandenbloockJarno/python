
import subprocess
with open("hosts_and_ports.txt") as hp_fd:
    hp_contents = hp_fd.readlines()
    for hp_pair in hp_contents:
        hp_host = hp_pair.rstrip("\n").split(":")
        IP_adress = hp_host[0]
        Poort_address = hp_host[1]
        with open("commands.txt") as fh:
                completed = subprocess.run("ssh " + str(IP_adress) + " -p " + str(Poort_address) , capture_output=True, text=True, shell=True, stdin=fh)
                   